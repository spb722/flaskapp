# -*- coding: utf-8 -*-
# @Time    : 18-02-2022 16:15
# @Author  : sachin
# @Email   : spb722@gmail.com
# @File    : app.py.py
# @Software: PyCharm
import flask

app = flask.Flask(__name__)


@app.route('/', methods=['GET'])
def home():
    return "in home "

if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0")
